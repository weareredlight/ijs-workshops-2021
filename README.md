# ISCAC - IJS Workshops 2021

## Sessions
- [First session](sessions/1.md)
- [Second session](sessions/2.md)
- [Third session](sessions/3.md)
- [Fourth session](sessions/4.md)
  - [API extras](sessions/4.1.md)
- [Sixth session](sessions/6.md)

## Running the backend

Install dependencies  
```bundle install```

Setup database  
```rails db:setup```

Start the server  
```rails server -p 5000```

## Running the frontend

Install dependencies  
```yarn install```

Start  
```yarn start```
