# Session #3

## Likes

- `rails g model Like post:references user:references`
- `rails g controller Likes`

- ```ruby
  # app/models/like.rb
  validates :user_id, uniqueness: { scope: :post_id }
  ```
- ```ruby
  # app/models/post.rb
  has_many :likes
  has_many :users_with_like, through: :likes, source: :user
  ```
- ```ruby
  # app/models/user.rb
  has_many :likes
  has_many :liked_posts, through: :likes, source: :post
  ```

- ```ruby
  # config/routes.rb
  resources :posts do
    resource :likes, only: [:create, :destroy]
  end
  ```

- ```ruby
  # app/controllers/likes_controller.rb
  class LikesController < ApplicationController
    before_action :authenticate_user!

    def create
      like = current_user.likes.new(post_id: params[:post_id])

      respond_to do |format|
        if like.save
          format.json { head :created }
        else
          format.json { render json: like.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      like = current_user.likes.find_by(post_id: params[:post_id])

      respond_to do |format|
        if like.present?
          like.destroy
          format.json { head :no_content }
        else
          format.json { head :not_found }
        end
      end
    end
  end
  ```

- ```ruby
  # app/views/posts/_post.json.jbuilder
  json.likes do
    json.count post.likes.count
    json.users post.users_with_like.map(&:username)
  end
  ```
