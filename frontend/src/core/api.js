import axios from 'axios';

import { get } from '../utils/localStorage';
import { API_BASE_URL } from '../utils/constants';

const api = axios.create({
  baseURL: API_BASE_URL,
  timeout: 150000,
});

// Add access token to every API request
api.interceptors.request.use(async (config) => {
  const accessToken = get('USER[ACCESS_TOKEN]');
  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: accessToken ? `Bearer ${accessToken}` : undefined,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
});

const apiConfig = {
  /*
    {
      user: {
        username: "test",
        email: "test@example.com",
        password: "123456",
        password_confirmation: "123456"
      }
    }
  */
  register(userData) {
    return api.post('/users', userData);
  },
  /*
    {
      user: {
        email: "test@example.com",
        password: "123456"
      }
    }
  */
  login(loginData) {
    return api.post('/users/sign_in', loginData);
  },
  getPosts() {
    return api.get('/posts');
  },
};

export default apiConfig;
