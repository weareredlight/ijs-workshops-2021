export const USERNAME_KEY = 'USER[USERNAME]';
export const TOKEN_KEY = 'USER[ACCESS_TOKEN]';

export const API_BASE_URL = 'http://localhost:5000';
// export const API_BASE_URL = 'http://ijs-workshop.dokku.rls-intra.com';
