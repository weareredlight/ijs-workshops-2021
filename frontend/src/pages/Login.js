import React, { useState } from 'react';

import { useUserContext } from '../hooks/useUser';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { login, error } = useUserContext();

  return (
    <div>
      <h1>Login</h1>
      <input
        type="text"
        placeholder="Write your email here"
        onChange={(ev) => setEmail(ev.target.value)}
      />
      <input
        type="password"
        placeholder="Write your password here"
        onChange={(ev) => setPassword(ev.target.value)}
      />
      <button onClick={() => login(email, password)}>Login</button>
      {error && 'Wrong credentials.'}
    </div>
  );
};

export default Login;
