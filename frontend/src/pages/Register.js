import React, { useState } from 'react';

import apiConfig from '../core/api';

// import useUser from '../hooks/useUser';

const Register = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState(null);

  const register = () => {
    apiConfig
      .register({
        user: {
          username,
          email,
          password,
          password_confirmation: password,
        },
      })
      .then(() => {
        setMessage({ type: 'success', text: 'User created with success.' });
        setUsername('');
        setEmail('');
        setPassword('');
      })
      .catch(() => {
        setMessage({ type: 'error', text: 'Something went wrong.' });
      });
  };

  return (
    <div>
      <h1>Register</h1>
      <input
        type="text"
        placeholder="Write your name here"
        onChange={(ev) => setUsername(ev.target.value)}
      />
      <input
        type="text"
        placeholder="Write your email here"
        onChange={(ev) => setEmail(ev.target.value)}
      />
      <input
        type="password"
        placeholder="Write your password here"
        onChange={(ev) => setPassword(ev.target.value)}
      />
      <button onClick={register}>Register</button>
      {message && message.text}
    </div>
  );
};

export default Register;
