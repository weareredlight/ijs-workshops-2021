import React from 'react';

import { useUserContext } from '../hooks/useUser';
import usePostListing from '../hooks/usePostListing';

import '../assets/stylesheets/pages/dashboard.css';
import '../assets/stylesheets/atoms/post.css';
import '../assets/stylesheets/atoms/profile_card.css';
import '../assets/stylesheets/atoms/comment.css';

import profileIcon from '../assets/images/profile.svg';
import likeIcon from '../assets/images/like.svg';
import commentIcon from '../assets/images/comment.svg';
import addPostIcon from '../assets/images/addpost.svg';
import searchIcon from '../assets/images/search.svg';

const Home = () => {
  const { isLoggedIn } = useUserContext();
  const { posts, isLoading, error } = usePostListing();

  if (isLoading) {
    return 'loading';
  }

  if (error) {
    return 'error';
  }

  return (
    <>
      <nav>
        <a href="/">
          <h1 className="logo">instaLight</h1>
        </a>
        <div>
          <a href="/new">
            <img src={addPostIcon} alt="Add post icon" />
            <span>Add post</span>
          </a>
          <a href="/search">
            <img src={searchIcon} alt="Search icon" />
            <span>Search</span>
          </a>

          {isLoggedIn && (
            <a href="/profile">
              <img src={profileIcon} alt="Profile icon" />
              <span>Profile</span>
            </a>
          )}
        </div>
      </nav>
      <main>
        <h2 className="main-title">Dashboard</h2>
        <section>
          {posts.map((post, index) => (
            <div className="post-card" key={index}>
              <div className="post-top">
                <a
                  href={`/users/${post.user.username}`}
                  className="profile-card"
                >
                  <img src={profileIcon} alt="Profile icon" />
                  <span>{post.user.username}</span>
                </a>
                <span>{post.time_ago}</span>
              </div>
              <a href={`/posts/${post.id}`}>
                <img src={post.picture.url} alt="Post placeholder" />
              </a>
              <div className="post-bottom">
                <div>
                  <img src={likeIcon} alt="Like icon" />
                  <img src={commentIcon} alt="Comment icon" />
                  <a href={`/posts/${post.id}`}>{post.likes.count} likes</a>
                  <a href={`/posts/${post.id}`}>
                    {post.comments.count} comments
                  </a>
                </div>
                <p className="comment">
                  <a href={`/users/${post.user.username}`}>
                    {post.user.username}
                  </a>
                  {post.description}
                </p>
              </div>
            </div>
          ))}
        </section>
      </main>
    </>
  );
};

export default Home;
