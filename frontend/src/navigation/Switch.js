import React from 'react';
import { Switch as RouterSwitch, Route, Redirect } from 'react-router-dom';

import { useUserContext } from '../hooks/useUser';

import routes from './routes';

const Switch = () => {
  const { isLoggedIn } = useUserContext();

  return (
    <RouterSwitch>
      {routes.map(
        ({ exact, path, component, needLogin, allowLoggedIn = true }) => {
          const Screen = component;

          return (
            <Route
              key={path}
              path={path}
              exact={exact}
              render={(props) => {
                if (needLogin) {
                  if (isLoggedIn) {
                    return <Screen {...props} />;
                  }
                  return <Redirect to="/login" />;
                } else if (!allowLoggedIn && isLoggedIn) {
                  return <Redirect to="/" />;
                }
                return <Screen {...props} />;
              }}
            />
          );
        }
      )}
    </RouterSwitch>
  );
};

export default Switch;
