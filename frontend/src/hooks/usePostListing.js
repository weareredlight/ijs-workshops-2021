import { useEffect, useState } from 'react';

import apiConfig from '../core/api';

const usePostListing = () => {
  const [posts, setPosts] = useState([]);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const getPosts = () => {
    setIsLoading(true);
    apiConfig
      .getPosts()
      .then((response) => {
        setPosts(response.data);
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    if (!posts.length) {
      getPosts();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    posts,
    error,
    isLoading,
  };
};

export default usePostListing;
