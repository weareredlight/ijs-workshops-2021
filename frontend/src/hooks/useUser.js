import React, { useContext, useState, useMemo } from 'react';

import apiConfig from '../core/api';
import { get, set, clear } from '../utils/localStorage';
import { USERNAME_KEY, TOKEN_KEY } from '../utils/constants';

const UserContext = React.createContext();

export const useUserContext = () => {
  const context = useContext(UserContext);

  if (!context) {
    throw new Error(
      'Clients of useUserContext must be wrapped inside a <UserContextProvider />'
    );
  }

  return context;
};

export const UserContextProvider = (props) => {
  const [username, setUsername] = useState(get(USERNAME_KEY));
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(!!username);

  const login = (email, password) => {
    setIsLoading(true);

    apiConfig
      .login({
        user: {
          email,
          password,
        },
      })
      .then((response) => {
        const token = response.headers.authorization;
        const { username } = response.data;

        set(TOKEN_KEY, token);
        set(USERNAME_KEY, username);

        setUsername(response.data.username);
        setIsLoggedIn(true);
      })
      .catch((error) => {
        clear();
        setError(error);
        setIsLoggedIn(false);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const context = useMemo(
    () => ({
      username,
      error,
      isLoading,
      isLoggedIn,
      login,
    }),
    [username, error, isLoading, isLoggedIn]
  );

  return <UserContext.Provider value={context} {...props} />;
};
