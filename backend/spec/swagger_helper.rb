# frozen_string_literal: true

require "rails_helper"

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.join("swagger").to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    "v1/swagger.yaml" => {
      openapi: "3.0.1",
      info: {
        title: "API V1",
        version: "v1",
      },
      paths: {},
      servers: [
        {
          url: "https://{defaultHost}",
          variables: {
            defaultHost: {
              default: "www.example.com",
            },
          },
        },
        {
          url: "http://{defaultHost}",
          variables: {
            defaultHost: {
              default: "localhost:5000",
            },
          },
        },
      ],
      components: {
        securitySchemes: {
          basic_auth: {
            type: :http,
            scheme: :bearer,
          },
        },
        schemas: {
          Post: {
            type: :object,
            properties: {
              id: { type: :integer },
              description: { type: :string },
              picture: {
                type: :object,
                properties: {
                  url: { type: :string },
                },
              },
              user_id: { type: :integer },
              user: {
                type: :object,
                properties: {
                  username: { type: :string },
                  id: { type: :integer },
                },
              },
              likes: {
                type: :object,
                properties: {
                  count: { type: :integer },
                  users: {
                    type: :array,
                    items: { type: :string },
                  },
                },
              },
              comments: {
                type: :object,
                properties: {
                  count: { type: :integer },
                  comments: {
                    type: :array,
                    items: {
                      type: :object,
                      properties: {
                        id: { type: :integer },
                        text: { type: :string },
                        username: { type: :string },
                      },
                    },
                  },
                },
              },
              time_ago: { type: :string },
              created_at: { type: :string },
              updated_at: { type: :string },
            },
          },
          PostParams: {
            type: :object,
            properties: {
              "post[description]": { type: :string },
              "post[picture]": { type: :file },
            },
            required: ["post[description]", "post[picture]"],
          },
          User: {
            type: :object,
            properties: {
              id: { type: :number },
              username: { type: :string },
              email: { type: :string },
              created_at: { type: :string },
              updated_at: { type: :string },
            },
          },
          UserRegisterParams: {
            type: :object,
            properties: {
              user: {
                type: :object,
                properties: {
                  username: { type: :string },
                  email: { type: :string },
                  password: { type: :string },
                  password_confirmation: { type: :string },
                },
                required: ["username", "email", "password", "password_confirmation"],
              },
            },
            required: ["user"],
          },
          UserLoginParams: {
            type: :object,
            properties: {
              user: {
                type: :object,
                properties: {
                  email: { type: :string },
                  password: { type: :string },
                },
                required: ["email", "password"],
              },
            },
            required: ["user"],
          },
          UserProfile: {
            type: :object,
            properties: {
              id: { type: :number },
              username: { type: :string },
              posts: {
                type: :array,
                items: { "$ref" => "#/components/schemas/Post" },
              },
            },
          },
          CommentParams: {
            type: :object,
            properties: {
              comment: {
                type: :object,
                properties: {
                  text: { type: :string },
                },
                required: ["comment"],
              },
            },
            required: ["comment"],
          },
        },
      },
    },
  }

  # Specify the format of the output Swagger file when running 'rswag:specs:swaggerize'.
  # The swagger_docs configuration option has the filename including format in
  # the key, this may want to be changed to avoid putting yaml in json files.
  # Defaults to json. Accepts ':json' and ':yaml'.
  config.swagger_format = :yaml
end
