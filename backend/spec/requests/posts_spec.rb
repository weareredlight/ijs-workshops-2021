# frozen_string_literal: true
require "swagger_helper"
require "devise/jwt/test_helpers"

RSpec.describe("posts", type: :request) do
  let(:user) do
    User.create(
      username: "user",
      email: "user@domain.com",
      password: "password",
      password_confirmation: "password"
    )
  end
  let(:token) do
    Devise::JWT::TestHelpers.auth_headers({}, user)["Authorization"]
  end
  let(:image) do
    image_path = Rails.root.join("spec", "fixtures", "image.jpg")
    Rack::Test::UploadedFile.new(image_path, "image/jpeg")
  end
  let(:posts) do
    [
      Post.create(description: "foo", user: user, picture: image),
      Post.create(description: "bar", user: user, picture: image),
    ]
  end

  path "/posts" do
    get("List all posts") do
      tags "Posts"

      consumes "application/json"
      produces "application/json"

      response(200, "successful") do
        schema type: :array, items: { "$ref": "#/components/schemas/Post" }

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end

    post("Add a new post") do
      tags "Posts"

      consumes "multipart/form-data"
      produces "application/json"

      security [basic_auth: []]

      parameter name: :post, in: :formData, schema: { "$ref": "#/components/schemas/PostParams" }, as: :post_

      response(201, "successful") do
        schema "$ref": "#/components/schemas/Post"

        let(:Authorization) { token }
        let(:post) { { description: "bar", picture: image } }

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end
  end

  path "/posts/{id}" do
    parameter name: :id, in: :path, type: :integer, description: "Post ID"

    get("Find post by ID") do
      tags "Posts"

      consumes "application/json"
      produces "application/json"

      response(200, "successful") do
        schema "$ref": "#/components/schemas/Post"

        let(:id) { posts[0].id }

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end

    put("Updates a post") do
      tags "Posts"

      consumes "multipart/form-data"
      produces "application/json"

      security [basic_auth: []]

      parameter name: :post, in: :formData, schema: { "$ref": "#/components/schemas/PostParams" }

      response(200, "successful") do
        schema "$ref": "#/components/schemas/Post"

        let(:Authorization) { token }
        let(:id) { posts[0].id }
        let(:post) { { description: "bar" } }

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end

    delete("Deletes a post") do
      tags "Posts"

      produces "application/json"
      security [basic_auth: []]

      response(204, "successful") do
        let(:Authorization) { token }
        let(:id) { posts[1].id }

        run_test!
      end
    end
  end
end
