# frozen_string_literal: true
require "rails_helper"
require "swagger_helper"

RSpec.describe("users", type: :request) do
  let(:registered_user) do
    User.create(
      username: "new_user",
      email: "new_user@domain.com",
      password: "password",
      password_confirmation: "password"
    )
  end

  path "/users" do
    post("Create user") do
      tags "Users"

      consumes "application/json"
      produces "application/json"

      parameter name: :user, in: :body, schema: { "$ref": "#/components/schemas/UserRegisterParams" }

      response(201, "successful") do
        schema "$ref": "#/components/schemas/User"

        let(:user) do
          {
            user: {
              username: "user2",
              email: "user2@domain.com",
              password: "password",
              password_confirmation: "password",
            },
          }
        end

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end

    get("Search for users") do
      tags "Users"

      consumes "application/json"
      produces "application/json"

      parameter name: :query, in: :query, type: :string

      response(200, "successful") do
        schema type: :array, items: { "$ref": "#/components/schemas/UserProfile" }

        let(:query) { "user" }

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end
  end

  path "/users/sign_in" do
    post("Logs user into the system") do
      tags "Users"

      consumes "application/json"
      produces "application/json"

      parameter name: :user, in: :body, schema: { "$ref": "#/components/schemas/UserLoginParams" }

      response(201, "successful") do
        schema "$ref": "#/components/schemas/User"

        let(:user) do
          {
            user: {
              email: registered_user.email,
              password: "password",
            },
          }
        end

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end
  end

  path "/users/{id}" do
    parameter name: :id, in: :path, type: :string, description: "Username"

    get("Get user profile") do
      tags "Users"

      consumes "application/json"
      produces "application/json"

      response(200, "successful") do
        schema "$ref": "#/components/schemas/UserProfile"

        let(:id) { registered_user.username }

        after do |example|
          example.metadata[:response][:content] = {
            "application/json" => {
              example: JSON.parse(response.body, symbolize_names: true),
            },
          }
        end

        run_test!
      end
    end
  end
end
