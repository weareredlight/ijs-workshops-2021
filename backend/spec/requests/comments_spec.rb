# frozen_string_literal: true
require "swagger_helper"
require "devise/jwt/test_helpers"

RSpec.describe("comments", type: :request) do
  let(:user) do
    User.create(
      username: "user",
      email: "user@domain.com",
      password: "password",
      password_confirmation: "password"
    )
  end
  let(:token) do
    Devise::JWT::TestHelpers.auth_headers({}, user)["Authorization"]
  end
  let(:saved_post) do
    image_path = Rails.root.join("spec", "fixtures", "image.jpg")
    image = Rack::Test::UploadedFile.new(image_path, "image/jpeg")
    Post.create(
      user: user,
      description: "foo bar",
      picture: image
    )
  end
  let(:saved_comment) do
    Comment.create(
      user: user,
      post: saved_post,
      text: "foo bar"
    )
  end

  path "/posts/{post_id}/comments" do
    parameter name: :post_id, in: :path, type: :integer, description: "Post ID"

    post("Add a comment") do
      tags "Comments"

      consumes "application/json"
      produces "application/json"

      security [basic_auth: []]

      parameter name: :comment, in: :body, schema: { "$ref": "#/components/schemas/CommentParams" }

      response(201, "successful") do
        let(:Authorization) { token }
        let(:post_id) { saved_post.id }

        let(:comment) do
          {
            comment: {
              text: "foo bar",
            },
          }
        end

        run_test!
      end
    end
  end

  path "/posts/{post_id}/comments/{id}" do
    parameter name: :post_id, in: :path, type: :integer, description: "Post ID"
    parameter name: :id, in: :path, type: :integer, description: "Comment ID"

    delete("Delete a comment") do
      tags "Comments"

      consumes "application/json"
      produces "application/json"

      security [basic_auth: []]

      response(204, "successful") do
        let(:Authorization) { token }
        let(:post_id) { saved_post.id }
        let(:id) { saved_comment.id }

        run_test!
      end
    end
  end
end
