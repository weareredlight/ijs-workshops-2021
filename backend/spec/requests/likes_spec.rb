# frozen_string_literal: true
require "swagger_helper"
require "devise/jwt/test_helpers"

RSpec.describe("likes", type: :request) do
  let(:user) do
    User.create(
      username: "user",
      email: "user@domain.com",
      password: "password",
      password_confirmation: "password"
    )
  end
  let(:token) do
    Devise::JWT::TestHelpers.auth_headers({}, user)["Authorization"]
  end
  let(:saved_post) do
    image_path = Rails.root.join("spec", "fixtures", "image.jpg")
    image = Rack::Test::UploadedFile.new(image_path, "image/jpeg")
    Post.create(
      user: user,
      description: "foo bar",
      picture: image
    )
  end

  path "/posts/{post_id}/likes" do
    parameter name: :post_id, in: :path, type: :integer, description: "Post ID"

    post("Like a post") do
      tags "Likes"

      consumes "application/json"
      produces "application/json"

      security [basic_auth: []]

      response(201, "successful") do
        let(:Authorization) { token }
        let(:post_id) { saved_post.id }

        run_test!
      end
    end

    delete("Delete a like") do
      tags "Likes"

      consumes "application/json"
      produces "application/json"

      security [basic_auth: []]

      response(204, "successful") do
        let!(:like) { saved_post.likes.create(user: user) }
        let(:Authorization) { token }
        let(:post_id) { saved_post.id }

        run_test!
      end
    end
  end
end
