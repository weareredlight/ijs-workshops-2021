# frozen_string_literal: true
class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    comment = current_user.comments.new(post_id: params[:post_id], text: comment_params[:text])

    respond_to do |format|
      if comment.save
        format.json { head(:created) }
      else
        format.json { render(json: comment.errors, status: :unprocessable_entity) }
      end
    end
  end

  def destroy
    comment = Comment.find_by(id: params[:id])

    respond_to do |format|
      if comment.present?
        if comment.user_id != current_user.id
          format.json { head(:forbidden) }
        else
          comment.destroy
          format.json { head(:no_content) }
        end
      else
        format.json { head(:not_found) }
      end
    end
  end

  protected

  def comment_params
    params.require(:comment).permit(:text)
  end
end
