# frozen_string_literal: true
class UsersController < ApplicationController
  def index
    @results = User.where("username LIKE ?", "%#{params[:query]}%")

    respond_to do |format|
      format.json { render(:index, status: :ok) }
    end
  end

  def show
    @user = User.find_by(username: params[:id])

    respond_to do |format|
      if @user.present?
        format.json { render(:show, status: :ok) }
      else
        format.json { head(:not_found) }
      end
    end
  end
end
