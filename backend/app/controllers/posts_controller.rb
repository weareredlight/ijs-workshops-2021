# frozen_string_literal: true
class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts or /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1 or /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    if @post.user_id != current_user.id
      respond_to do |format|
        format.html { redirect_to(@post, notice: "You don't have permissions.") }
        format.json { head(:forbidden) }
      end
    end
  end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)
    @post.user = current_user

    respond_to do |format|
      if @post.save
        format.html { redirect_to(@post, notice: "Post was successfully created.") }
        format.json { render(:show, status: :created, location: @post) }
      else
        format.html { render(:new, status: :unprocessable_entity) }
        format.json { render(json: @post.errors, status: :unprocessable_entity) }
      end
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.user_id != current_user.id
        format.html { redirect_to(@post, notice: "You don't have permissions.") }
        format.json { head(:forbidden) }
      elsif @post.update(post_params)
        format.html { redirect_to(@post, notice: "Post was successfully updated.") }
        format.json { render(:show, status: :ok, location: @post) }
      else
        format.html { render(:edit, status: :unprocessable_entity) }
        format.json { render(json: @post.errors, status: :unprocessable_entity) }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    respond_to do |format|
      if @post.user_id != current_user.id
        format.html { redirect_to(@post, notice: "You don't have permissions.") }
        format.json { head(:forbidden) }
      else
        @post.destroy
        format.html { redirect_to(posts_url, notice: "Post was successfully destroyed.") }
        format.json { head(:no_content) }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:description, :picture, :remote_picture_url)
  end
end
