# frozen_string_literal: true
class LikesController < ApplicationController
  before_action :authenticate_user!

  def create
    like = current_user.likes.new(post_id: params[:post_id])

    respond_to do |format|
      if like.save
        format.json { head(:created) }
      else
        format.json { render(json: like.errors, status: :unprocessable_entity) }
      end
    end
  end

  def destroy
    like = current_user.likes.find_by(post_id: params[:post_id])

    respond_to do |format|
      if like.present?
        like.destroy
        format.json { head(:no_content) }
      else
        format.json { head(:not_found) }
      end
    end
  end
end
