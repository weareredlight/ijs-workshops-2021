# frozen_string_literal: true
class Post < ApplicationRecord
  mount_uploader :picture, PictureUploader

  # Associations
  belongs_to :user
  has_many :likes
  has_many :users_with_like, through: :likes, source: :user
  has_many :comments
end
