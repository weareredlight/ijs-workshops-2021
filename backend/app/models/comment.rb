# frozen_string_literal: true
class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  # Validations
  validates :text, presence: true
end
