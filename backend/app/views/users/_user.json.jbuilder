# frozen_string_literal: true
json.extract!(user, :id, :username)
json.posts do
  json.array! user.posts.map do |post|
    json.partial!("posts/post", post: post)
  end
end
