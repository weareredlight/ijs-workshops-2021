# frozen_string_literal: true
json.array!(@results, partial: "users/user", as: :user)
