# frozen_string_literal: true
json.extract!(post, :id, :description, :picture, :user_id, :created_at, :updated_at)
json.time_ago("#{time_ago_in_words(post.created_at)} ago")
json.user do
  json.username(post.user.username)
  json.id(post.user.id)
end
json.likes do
  json.count(post.likes.count)
  json.users(post.users_with_like.map(&:username))
end
json.comments do
  json.count(post.comments.count)
  json.comments(post.comments.map { |comment| { id: comment.id, text: comment.text, username: post.user.username } })
end
