# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.create(
  username: "dinis",
  email: "dinis@redlight.dev",
  password: "password",
  password_confirmation: "password"
)
user2 = User.create(
  username: "samuel",
  email: "samuelmn@redlight.dev",
  password: "password",
  password_confirmation: "password"
)
user3 = User.create(
  username: "renato",
  email: "r@redlight.dev",
  password: "password",
  password_confirmation: "password"
)
user4 = User.create(
  username: "miguel",
  email: "miguelv@redlight.dev",
  password: "password",
  password_confirmation: "password"
)

post1 = Post.create(
  user: user1,
  description: "Hic ipsum nostrum saepe vero est praesentium tempora.",
  remote_picture_url: "https://images.unsplash.com/photo-1621414130808-289413f32292"
)
post2 = Post.create(
  user: user2,
  description: "Aut non saepe distinctio perspiciatis nihil non labore omnis.",
  remote_picture_url: "https://images.unsplash.com/photo-1621414130936-6f0c63360ec2"
)
post3 = Post.create(
  user: user3,
  description: "Nihil accusamus animi corporis itaque autem voluptatibus.",
  remote_picture_url: "https://images.unsplash.com/photo-1621375041819-8799d4a172ed"
)
post4 = Post.create(
  user: user4,
  description: "Voluptatem quia porro delectus.",
  remote_picture_url: "https://images.unsplash.com/photo-1618939536400-d25c3d7b7340"
)

Comment.create(
  user: user1,
  post: post4,
  text: "Aperiam accusantium occaecati omnis dolores sit sequi provident."
)
Comment.create(
  user: user2,
  post: post4,
  text: "Est aut impedit natus est nulla quae."
)
Comment.create(
  user: user1,
  post: post2,
  text: "Debitis consequuntur eaque porro provident sit."
)

Like.create(
  user: user4,
  post: post1
)
Like.create(
  user: user1,
  post: post3
)
Like.create(
  user: user2,
  post: post3
)
Like.create(
  user: user4,
  post: post3
)
